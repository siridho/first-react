import React from "react";
import axios, { post } from "axios";

class Form extends React.Component {
 constructor(props) {
  super(props);
  this.state = {
   file: null,
  };
  this.onFormSubmit = this.onFormSubmit.bind(this);
  this.onChange = this.onChange.bind(this);
  this.fileUpload = this.fileUpload.bind(this);
 }
 onFormSubmit(e) {
  e.preventDefault(); // Stop form submit

  this.fileUpload(this.state.file).then((response) => {
   console.log(response.data);
  });
 }
 onChange(e) {
  this.setState({ file: e.target.files[0] });
 }
 fileUpload(file) {
  const url = "localhost:7000/update/";
  const formData = new FormData();
  formData.append("file", file);
  const config = {
   headers: {
    "content-type": "multipart/form-data",
   },
  };
  return post(url, formData, config);
 }

 render() {
  return (
   <container>
    <form onSubmit={this.onFormSubmit}>
     <div class="form-group">
      <label>Name</label>
      <input type="text" class="form-control" name="name" />
     </div>
     <div class="form-group">
      <label>SKU</label>
      <input type="text" class="form-control" name="sku" />
     </div>
     <div class="form-group">
      <label>price</label>
      <input type="number" class="form-control" name="price" />
     </div>
     <div class="form-group">
      <label>Description</label>
      <textarea class="form-control" rows="3"></textarea>
     </div>
     <div class="form-group">
      <label for="">Image</label>
      <input type="file" class="form-control" onChange={this.onChange} />
     </div>

     <button type="submit">Upload</button>
    </form>
   </container>
  );
 }
}

export default Form;
