import React from "react";
import axios from "axios";

import { Link } from "react-router";

class Home extends React.Component {
 state = {
  products: [],
 };

 componentDidMount() {
  axios.get(`http://localhost:7000/products`).then((res) => {
   const { products } = res.data;
   console.log(products);
   this.setState({ products });
  });
  // this.props.ProductStore.getProductsAsync();
 }

 deleteProduct(id) {
  axios.post(`http://localhost:7000/delete/${id}`).then((res) => {
   alert("success delete data");
   const products = this.state.products.filter((item) => item.id !== id);
   this.setState({ products });
  });
 }

 render() {
  const { products } = this.state;
  return (
   <div class="container">
    <div class="row">
     {products.map((product) => {
      const splitDescription = product.description.split("</p>");
      const [firstParagraph] = splitDescription;
      return (
       <div class="col-sm-4" style={{ paddingTop: "10px" }}>
        <div class="card float-left">
         <div class="row">
          <div class="col-sm-12">
           <div class="card-block">
            <h4 class="card-title">{product.name}</h4>
            <div>
             {" "}
             <img
              class="d-block w-100"
              src={"http://" + product.image}
              alt=""
             />
            </div>
            <div
             dangerouslySetInnerHTML={{ __html: `${firstParagraph}</p>` }}
            />
            <button
             class="btn btn-danger"
             onClick={() => this.deleteProduct(product.id)}
            >
             delete
            </button>
            <a href="product" class="btn btn-primary">
             edit
            </a>
           </div>
          </div>

          {/* <div class="col-sm-5">
           <img class="d-block w-100" src={"http://" + product.image} alt="" />
          </div> */}
         </div>
        </div>
       </div>
      );
     })}
    </div>
   </div>
  );
 }
}

export default Home;
